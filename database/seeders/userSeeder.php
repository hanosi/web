<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'no_identitas' => '1915051104',
                'nama' => 'Hanosi Wazri',
                'prodi'=> 'Pendidikan Teknik Informatika',
                'jurusan'=> 'Teknik Informatika',
                'fakultas'=> 'Fakultas Teknik dan Kejuruan',
                'email'=> 'hanosi@undiksha.ac.id',
                'password'=> bcrypt('123'),
                'level'=>'Staff Fakultas',
                'image'=> '150-1.jpg',
                'status_akun'=>'Aktif',
            ],
            [
                'no_identitas' => '1915051060',
                'nama' => 'Tri Sukma',
                'prodi'=> 'Pendidikan Teknik Informatika',
                'jurusan'=> 'Teknik Informatika',
                'fakultas'=> 'Fakultas Teknik dan Kejuruan',
                'email'=> 'ayu.tri.sukma@undiksha.ac.id',
                'password'=> bcrypt('123'),
                'level'=>'Mahasiswa',
                'image'=> '150-6.jpg',
                'status_akun'=>'Aktif',
            ],
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
