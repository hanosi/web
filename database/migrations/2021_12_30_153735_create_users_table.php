<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_identitas')->unique();
            $table->string('nama');
            $table->string('prodi');
            $table->string('jurusan');
            $table->string('fakultas');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('level',['Mahasiswa','Staff Fakultas'])->default('Mahasiswa');
            $table->string('image')->nullable();
            $table->enum('status_akun',['Aktif','Tidak Aktif'])->default('Tidak Aktif');
            $table->string('token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
