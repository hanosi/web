<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAkunZoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akun_zoom', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('id_zoom')->unique()->random_int(100000, 999999);
            $table->string('nama_akun');
            $table->string('email_zoom');
            $table->integer('kapasitas');
            $table->enum('tipe_akun',['Zoom Meetings','Zoom Phones','Zoom Events & Webinar','Zoom Rooms','Zoom United'])->default('Zoom Meetings');
            $table->string('link')->default('https://www.zoom.us/start/webmeeting');
            $table->enum('status_zoom',['Aktif','Tidak Aktif'])->default('Tidak Aktif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akun_zoom');
    }
}
