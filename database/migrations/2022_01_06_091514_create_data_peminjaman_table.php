<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataPeminjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_peminjaman', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('no_identitas');
            $table->string('nama_kegiatan');
            $table->string('deskripsi');
            $table->enum('status',['Disetujui','Ditolak','Dibatalkan','Menunggu Konfirmasi'])->default('Menunggu Konfirmasi');
            $table->dateTimeTz('start');
            $table->dateTimeTz('end');
            $table->integer('id_zoom');
            $table->foreign('id_zoom')->references('id_zoom')->on('akun_zoom')->onDelete('cascade');
            $table->foreign('no_identitas')->references('no_identitas')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_peminjaman');
    }
}
