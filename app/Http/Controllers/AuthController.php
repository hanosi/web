<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\dataController;
use App\Models\User;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth/login');
    }
    public function registration()
    {
        return view('auth/registration');
    }

    public function prosesRegistration(Request $request)
    {   
        $request->validate([
            'no_identitas' => 'required',
            'nama' => 'required',
            'prodi' => 'required',
            'jurusan' => 'required',
            'fakultas' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);
 
        DB::table('users')->insert([
            'id' => Null,
            'no_identitas' => $request->no_identitas,
            'nama' => $request->nama,
            'prodi' => $request->prodi,
            'jurusan' => $request->jurusan,
            'fakultas' => $request->fakultas,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'created_at'=> date("Y-m-d H:i:s"),
            'updated_at'=> date("Y-m-d H:i:s"),
        ]);

        return redirect('/');
    }

    public function home(Request $request)
    {
        $data = DB::table('data_peminjaman')
            ->join('users', 'data_peminjaman.no_identitas', '=', 'users.no_identitas')
            ->get();
        
        request()->validate([
        'email' => 'required',
        'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if ($user->level == 'Staff Fakultas' and $user->status_akun == 'Aktif') {
                return view('fakultas.index',['data' => $data]);
            } elseif ($user->level == 'Mahasiswa' and $user->status_akun == 'Aktif') {
                return view('mahasiswa.index',['data' => $data]);
            }
            return redirect('/');
        }
        return redirect('/')->withSuccess('Oppes! Silahkan Cek Inputanmu');
    }

    public function ForgetPassword()
    {
        return view('auth/forgetPassword');
    }

    public function logout(Request $request) {
        $request->session()->flush();
        Auth::logout();
        return Redirect('/');
    }
}
