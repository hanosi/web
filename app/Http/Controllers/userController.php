<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\Enum;
use phpDocumentor\Reflection\Types\Null_;
use Illuminate\Support\Facades\Auth;

class userController extends Controller
{
    public function dataPeminjaman()
    {
    	// mengambil data dari table peminjaman
    	$data = DB::table('data_peminjaman')
            ->join('users', 'data_peminjaman.no_identitas', '=', 'users.no_identitas')
            ->get();
 
    	// mengirim data ke view index
    	return view('/mahasiswa/data',['data' => $data]);
 
    }
}
