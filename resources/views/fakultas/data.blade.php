@extends('layout.templateFakultas')

@section('Header')
    Peminjaman Akun ZOOM
@endsection

@section('titleNav')
    Data Peminjaman ZOOM
@endsection

@section('content')
@csrf
<div class="post d-flex flex-column-fluid" id="kt_post">
    <!--begin::Container-->
    <div id="kt_content_container" class="container-xxl">
        <!--begin::Card-->
        <div class="card">
            <!--begin::Card header-->
            <div class="card-header border-0 pt-6">
                <!--begin::Card title-->
                <div class="card-title">
                    <!--begin::Search-->
                    <div class="d-flex align-items-center position-relative my-1">
                        <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                        <span class="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1"
                                    transform="rotate(45 17.0365 15.1223)" fill="black"></rect>
                                <path
                                    d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                    fill="black"></path>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <input type="text" data-kt-user-table-filter="search"
                            class="form-control form-control-solid w-250px ps-14" placeholder="Search user">
                    </div>
                    <!--end::Search-->
                </div>
                <!--begin::Card title-->
                <!--begin::Card toolbar-->
                <div class="card-toolbar">
                    <!--begin::Toolbar-->
                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                    </div>
                    <!--end::Toolbar-->
                </div>
                <!--end::Card toolbar-->
            </div>
            <!--end::Card header-->
            <!--begin::Card body-->
            <div class="card-body py-4">
                <!--begin::Table-->
                <div id="kt_table_users_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="table-responsive">
                        <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                            id="kt_table_users">
                            <!--begin::Table head-->
                            <thead>
                                <!--begin::Table row-->
                                <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
                                    <th class="w-10px pe-2 sorting_disabled" rowspan="1" colspan="1" aria-label=""
                                        style="width: 29.25px;">
                                        <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                            #
                                        </div>
                                    </th>
                                    <th class="min-w-125px" tabindex="0" aria-controls="kt_table_users" rowspan="1"
                                        colspan="1" aria-label="Nama: activate to sort column ascending"
                                        style="width: 254.953px; text-align:center;">Nama</th>
                                    <th class="min-w-125px" tabindex="0" aria-controls="kt_table_users" rowspan="1"
                                        colspan="1" aria-label="NIM/NIP: activate to sort column ascending"
                                        style="width: 50px; text-align:center;">Nama Kegiatan</th>
                                    <th class="min-w-125px" tabindex="0" aria-controls="kt_table_users" rowspan="1"
                                        colspan="1" aria-label="Status: activate to sort column ascending"
                                        style="width: 100px; text-align:center;">Status</th>
                                    <th class="min-w-125px" tabindex="0" aria-controls="kt_table_users" rowspan="1"
                                        colspan="1" aria-label="Time Start: activate to sort column ascending"
                                        style="width: 125px; text-align:center;">Waktu Start</th>
                                    <th class="min-w-125px" tabindex="0" aria-controls="kt_table_users" rowspan="1"
                                        colspan="1" aria-label="Time End: activate to sort column ascending"
                                        style="width: 141.297px; text-align:center;">Waktu Berakhir</th>
                                    <th class="text-end min-w-100px" rowspan="1" colspan="1" aria-label="Actions"
                                        style="width: 100px; text-align:center;">Actions
                                    </th>
                                </tr>
                                <!--end::Table row-->
                            </thead>
                            <!--end::Table head-->
                            <!--begin::Table body-->
                            <tbody class="text-gray-600 fw-bold">
                                <!--end::Table row-->
                                <?php
                                $no=1;
                                ?>
                                @foreach ($data as $d)
                                <tr class="odd">
                                    <td>
                                        <div class="form-check form-check-sm form-check-custom form-check-solid">
                                            <?php echo $no; $no++;?>
                                        </div>
                                    </td>
                                    <td class="d-flex align-items-center">
                                        <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                            <a href="#">
                                                <div class="symbol-label">
                                                    <img src="demo1/media/avatars/{{ $d->image }}" alt="Emma Smith"
                                                        class="w-100">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-800 text-hover-primary mb-1">{{ $d->nama }}</a>
                                            <span>{{ $d->email }}</span>
                                        </div>
                                    </td>
                                    <td style="text-align: center;">{{ $d->nama_kegiatan }}</td>
                                    <td data-order="2021-12-28T18:34:32+08:00" style="text-align: center;">
                                        <div class="badge badge-light fw-bolder" <?php if ($d->status=='Menunggu Konfirmasi'){echo "style='background-color: grey;'";}else if($d->status=='Disetujui'){echo "style='background-color: green;'";}else if($d->status=='Ditolak'){echo "style='background-color: red;'";}else if($d->status=='Dibatalkan'){echo "style='background-color: red;'";}?>>
                                            {{ $d->status }}</div>
                                    </td>
                                    <td data-order="2021-04-15T06:43:00+08:00">{{ $d->start }}</td>
                                    <td data-order="2021-04-15T06:43:00+08:00">{{ $d->end }}</td>
                                    <td class="text-end">
                                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm"
                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                            <span class="svg-icon svg-icon-5 m-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none">
                                                    <path
                                                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                        fill="black"></path>
                                                </svg>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </a>
                                        <!--begin::Menu-->
                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
                                            data-kt-menu="true">
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-3">
                                                <a href=<?php if ($d->status == 'Menunggu Konfirmasi'){ echo "/peminjaman/statusUpdate/".$d->id;}else if ($d->status == 'Disetujui'){echo "/peminjaman/status/".$d->id;}?>
                                                    class="menu-link px-3"><?php if ( $d->status == 'Menunggu Konfirmasi'){echo 'Setujui';}else if($d->status == 'Disetujui'){echo'Batalkan';}?></a>
                                            </div>
                                            <!--end::Menu item-->
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-3" <?php if ($d->status == 'Menunggu Konfirmasi'){echo 'show';}else{echo 'hidden';} ?>>
                                                <a href="/tolakPeminjaman/{{ $d->id }}"
                                                    class="menu-link px-3"
                                                    data-kt-users-table-filter="tolak">Tolak</a>
                                            </div>
                                            <!--end::Menu item-->
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-3">
                                                <a href="/peminjaman/hapus/{{ $d->id }}"
                                                    class="menu-link px-3"
                                                    data-kt-users-table-filter="delete_row">Delete</a>
                                            </div>
                                            <!--end::Menu item-->
                                        </div>
                                        <!--end::Menu-->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <!--end::Table body-->
                        </table>
                    </div>
                    <div class="row">
                        <div
                            class="col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start">
                        </div>
                        <div
                            class="col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end">
                            <div class="dataTables_paginate paging_simple_numbers" id="kt_table_users_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button page-item previous disabled"
                                        id="kt_table_users_previous"><a href="#" aria-controls="kt_table_users"
                                            data-dt-idx="0" tabindex="0" class="page-link"><i class="previous"></i></a>
                                    </li>
                                    <li class="paginate_button page-item active"><a href="#"
                                            aria-controls="kt_table_users" data-dt-idx="1" tabindex="0"
                                            class="page-link">1</a></li>
                                    <li class="paginate_button page-item next" id="kt_table_users_next">
                                        <a href="#" aria-controls="kt_table_users" data-dt-idx="4" tabindex="0"
                                            class="page-link"><i class="next"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Table-->
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
</div>
@endsection