@extends('layout.templateMahasiswa')

@section('Header')
Peminjaman Akun ZOOM
@endsection

@section('titleNav')
Data Peminjaman ZOOM
@endsection

@section('content')
@csrf
<div class="post d-flex flex-column-fluid" id="kt_post">
    <!--begin::Container-->
    <div id="kt_content_container" class="container-xxl">
        <!--begin::Card-->
        <div class="card">
            <!--begin::Card body-->
            <div class="card-body">
                <!--begin::Heading-->
                <div class="card-px text-center pt-15 pb-15">
                <div class="text-center pb-15 px-5">
                    <img src="https://i.ibb.co/c8jZpJb/undiksha.png" alt=""
                        class="mw-100 h-100px h-sm-170px">
                    </div>
                    <!--begin::Title-->
                    <h2 class="fs-2x fw-bolder mb-0">SISTEM INFORMASI PEMINJAMAN ZOOM</h2>
                </div>
                <!--end::Heading-->
                <!--begin::Illustration-->
                <!--end::Illustration-->
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
</div>
@endsection