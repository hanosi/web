<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sistem Informasi | @yield('header')</title>
    <meta charset="utf-8">
    <meta name="description"
        content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free.">
    <meta name="keywords"
        content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title"
        content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme">
    <meta property="og:url" content="https://keenthemes.com/metronic">
    <meta property="og:site_name" content="Keenthemes | Metronic">
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8">
    <link rel="shortcut icon" href="https://i.ibb.co/c8jZpJb/undiksha.png">
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="/demo1/plugins/global/plugins.dark.bundle.css" rel="stylesheet" type="text/css">
    <link href="/demo1/css/style.dark.bundle.css" rel="stylesheet" type="text/css">
    <!--end::Global Stylesheets Bundle-->
    <!--Begin::Google Tag Manager -->
    <script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script>
    <script type="text/javascript" async="" src="//www.googleadservices.com/pagead/conversion_async.js"></script>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script type="text/javascript" async=""
        src="https://www.googletagmanager.com/gtag/js?id=G-CDVH4VH813&amp;l=dataLayer&amp;cx=c"></script>
    <script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-5FS8GGP"></script>
    <script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&amp;l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-5FS8GGP');
    </script>
    <!--End::Google Tag Manager -->
    <script
        src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/984933920/?random=1640870488807&amp;cv=9&amp;fst=1640870488807&amp;num=1&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=768&amp;u_w=1366&amp;u_ah=720&amp;u_aw=1366&amp;u_cd=24&amp;u_his=4&amp;u_tz=480&amp;u_java=false&amp;u_nplug=5&amp;u_nmime=2&amp;gtm=2wgc10&amp;sendb=1&amp;ig=1&amp;frm=0&amp;url=https%3A%2F%2Fpreview.keenthemes.com%2Fmetronic8%2Fdemo1%2Fdark%2Fauthentication%2Flayouts%2Fbasic%2Fsign-in.html&amp;ref=https%3A%2F%2Fpreview.keenthemes.com%2Fmetronic8%2Fdemo1%2Fdark%2Fmodals%2Fforms%2Fnew-target.html&amp;tiba=Metronic%20-%20the%20world's%20%231%20selling%20Bootstrap%20Admin%20Theme%20Ecosystem%20for%20HTML%2C%20Vue%2C%20React%2C%20Angular%20%26%20Laravel%20by%20Keenthemes&amp;hn=www.googleadservices.com&amp;async=1&amp;rfmt=3&amp;fmt=4">
    </script>
    <script src="chrome-extension://mooikfkahbdckldjjndioackbalphokd/assets/prompt.js"></script>
</head>

<body id="kt_body" class="dark-mode">
    <!--Begin::Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FS8GGP" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!--End::Google Tag Manager (noscript) -->
    <!--begin::Main-->
    <!--begin::Root-->
    @yield('content')
    <!--end::Root-->
    <!--end::Main-->
    <!--begin::Javascript-->
    <script>
    var hostUrl = "/demo1/";
    </script>
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="/demo1/plugins/global/plugins.bundle.js"></script>
    <script src="/demo1/js/scripts.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="/demo1/js/custom/authentication/sign-in/general.js"></script>
    <!--end::Page Custom Javascript-->
    <!--end::Javascript-->

    <!--end::Body-->
    <svg id="SvgjsSvg1001" width="2" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.dev" style="overflow: hidden; top: -100%; left: -100%; position:absolute; opacity: 0;">
        <defs id="SvgjsDefs1002"></defs>
        <polyline id="SvgjsPolyline1003" points="0,0"></polyline>
        <path id="SvgjsPath1004" d="M0 0 "></path>
    </svg>
    <script type="text/javascript" id="">
    ! function(b, e, f, g, a, c, d) {
        b.fbq || (a = b.fbq = function() {
                a.callMethod ? a.callMethod.apply(a, arguments) : a.queue.push(arguments)
            }, b._fbq || (b._fbq = a), a.push = a, a.loaded = !0, a.version = "2.0", a.queue = [], c = e
            .createElement(f), c.async = !0, c.src = g, d = e.getElementsByTagName(f)[0], d.parentNode.insertBefore(
                c, d))
    }(window, document, "script", "https://connect.facebook.net/en_US/fbevents.js");
    fbq("init", "738802870177541");
    fbq("track", "PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=738802870177541&amp;ev=PageView&amp;noscript=1"></noscript>
    <script type="text/javascript" id="">
    try {
        (function() {
            var a = google_tag_manager["GTM-5FS8GGP"].macro(8);
            a = "undefined" == typeof a ? google_tag_manager["GTM-5FS8GGP"].macro(9) : a;
            var b = new Date;
            b.setTime(b.getTime() + 18E5);
            var c = "gtm-session-start";
            b = b.toGMTString();
            var d = "/",
                e = ".keenthemes.com";
            document.cookie = c + "\x3d" + a + "; Expires\x3d" + b + "; domain\x3d" + e + "; Path\x3d" + d
        })()
    } catch (a) {};
    </script>
    <script type="text/javascript" id="">
    (function() {
        var a = google_tag_manager["GTM-5FS8GGP"].macro(10) - 0 + 1,
            b = ".keenthemes.com";
        document.cookie = "damlPageCount\x3d" + a + ";domain\x3d" + b + ";path\x3d/;"
    })();
    </script>
</body>

</html>