@extends('layout.templateFakultas')

@section('Header')
Tambah Akun ZOOM
@endsection

@section('titleNav')
Tambah Akun ZOOM
@endsection

@section('content')
<div class="modal-dialog modal-dialog-centered mw-650px">
    <!--begin::Modal content-->
    <div class="modal-content rounded">
        <!--begin::Modal header-->
        <div class="modal-header pb-0 border-0 justify-content-end">
            <!--begin::Close-->
            <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                <a onclick="goBack()">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                transform="rotate(-45 6 17.3137)" fill="black"></rect>
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                fill="black"></rect>
                        </svg>
                    </span>
                </a>
                <!--end::Svg Icon-->
            </div>
            <!--end::Close-->
        </div>
        <!--begin::Modal header-->
        <!--begin::Modal body-->
        <div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
            <!--begin:Form-->
            @foreach($data as $d)
            <form id="kt_modal_new_target_form" class="form fv-plugins-bootstrap5 fv-plugins-framework"
                action="/akunZoom/edit" method="POST">
                {{ csrf_field() }}
                <!--begin::Heading-->
                <div class="mb-13 text-center">
                    <!--begin::Title-->
                    <h1 class="mb-3">Tambahkan Data Akun Zoom</h1>
                    <!--end::Title-->
                </div>
                <!--end::Heading-->
                <input type="hidden" name="id_zoom" value="{{ $d->id_zoom }}"> <br/>
                <!--begin::Input group-->
                <div class="d-flex flex-column mb-8 fv-row fv-plugins-icon-container">
                    <!--begin::Label-->
                    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                        <span class="required">Nama Akun</span>
                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title=""
                            data-bs-original-title="Specify a target name for future usage and reference"
                            aria-label="Specify a target name for future usage and reference"></i>
                    </label>
                    <!--end::Label-->
                    <input type="text" class="form-control form-control-solid"
                        name="nama_akun" id="nama" placeholder="Masukkan Nama Akun" value="{{ $d->nama_akun }}"></input>
                    <div class="fv-plugins-message-container invalid-feedback"></div>
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="d-flex flex-column mb-8 fv-row fv-plugins-icon-container">
                    <!--begin::Label-->
                    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                        <span class="required">Email ZOOM</span>
                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title=""
                            data-bs-original-title="Specify a target name for future usage and reference"
                            aria-label="Specify a target name for future usage and reference"></i>
                    </label>
                    <!--end::Label-->
                    <input type="email" class="form-control form-control-solid" placeholder="Masukkan Email" name="email_zoom"
                        id="nama_kegiatan" value="{{ $d->email_zoom }}">
                    <div class="fv-plugins-message-container invalid-feedback"></div>
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-6 fv-row fv-plugins-icon-container">
                        <label class="required fs-6 fw-bold mb-2">Kapasitas</label>
                        <!--begin::Input-->
                        <div class="position-relative d-flex align-items-center">
                            <!--begin-->
                            <input type="number" class="form-control form-control-solid" placeholder="Kapasitas Participant" name="kapasitas" value="{{ $d->kapasitas }}">
                            <!--end-->
                        </div>
                        <!--end::Input-->
                        <div class="fv-plugins-message-container invalid-feedback"></div>
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-6 fv-row">
                        <label class="required fs-6 fw-bold mb-2">Tipe Akun</label>
                        <!--begin::Input-->
                        <div class="position-relative d-flex align-items-center">
                            <!--begin::Datepicker-->
                            <select type="text" class="form-control form-control-solid" placeholder="Tipe Akun" name="tipe_akun" value="">
                                <option <?php if ($d->tipe_akun == 'Zoom Meetings'){echo 'Selected';}?>>Zoom Meetings</option>
                                <option <?php if ($d->tipe_akun == 'Zoom Phones'){echo 'Selected';}?>>Zoom Phones</option>
                                <option <?php if ($d->tipe_akun == 'Zoom Event & Webinar'){echo 'Selected';}?>>Zoom Event & Webinar</option>
                                <option <?php if ($d->tipe_akun == 'Zoom Rooms'){echo 'Selected';}?>>Zoom Rooms</option>
                                <option <?php if ($d->tipe_akun == 'Zoom United'){echo 'Selected';}?>>Zoom United</option>
                                <!-- @foreach ($data as $data)
                                <option value="{{ $data->id }}">{{ $data->nama_akun }}</option>
                                @endforeach -->

                            </select>
                            <!--end::Datepicker-->
                        </div>
                        <!--end::Input-->
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Input group-->
                <!--begin::Actions-->
                <div class="text-center">
                    <button type="reset" id="kt_modal_new_target_cancel" class="btn btn-light me-3">Reset</button>
                    <button type="submit" id="kt_modal_new_target_submit" class="btn btn-primary">
                        <span class="indicator-label">Submit</span>
                        <span class="indicator-progress">Please wait...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
                <!--end::Actions-->
                <div></div>
            </form>
            @endforeach
            <!--end:Form-->
        </div>
        <!--end::Modal body-->
    </div>
    <!--end::Modal content-->
</div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
function isi_otomatis() {
    var no_identitas = $("#no_identitas").val();
    $.ajax({
        url: 'ajax.php',
        data: "no_identitas=" + no_identitas,
    }).success(function(data) {
        var json = data,
            obj = JSON.parse(json);
        $('#nama').val(obj.nama);
    });
}
</script>
<script>
function goBack() {
    window.history.back();
}
</script>